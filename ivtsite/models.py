from django.db import models

class Contact(models.Model):
	name=models.Charfield(max_length=255, verbose_name=u"Имя")
	email=models.EmailField(null=True, blank=True)
	mobile=model.Charfield(max_length=255, verbose_name=u"Мобильный номер", null=True, blank=True)
	work=model.Charfield(max_length=255, verbose_name=u"Рабочий номер", null=True, blank=True)